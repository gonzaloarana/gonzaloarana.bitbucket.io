var files_dup =
[
    [ "check_distance.py", "check__distance_8py.html", [
      [ "Check_Distance", "classcheck__distance_1_1Check__Distance.html", "classcheck__distance_1_1Check__Distance" ]
    ] ],
    [ "closedloopcontroller.py", "closedloopcontroller_8py.html", [
      [ "ClosedLoopController", "classclosedloopcontroller_1_1ClosedLoopController.html", "classclosedloopcontroller_1_1ClosedLoopController" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "motor.py", "motor_8py.html", [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "ultrasonic.py", "ultrasonic_8py.html", [
      [ "Ultrasonic", "classultrasonic_1_1Ultrasonic.html", "classultrasonic_1_1Ultrasonic" ]
    ] ],
    [ "update_debouncing.py", "update__debouncing_8py.html", [
      [ "Update_Debouncing", "classupdate__debouncing_1_1Update__Debouncing.html", "classupdate__debouncing_1_1Update__Debouncing" ]
    ] ],
    [ "update_motor.py", "update__motor_8py.html", [
      [ "Update_Motor", "classupdate__motor_1_1Update__Motor.html", "classupdate__motor_1_1Update__Motor" ]
    ] ],
    [ "update_spintray.py", "update__spintray_8py.html", [
      [ "Update_Spintray", "classupdate__spintray_1_1Update__Spintray.html", "classupdate__spintray_1_1Update__Spintray" ]
    ] ]
];