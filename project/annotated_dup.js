var annotated_dup =
[
    [ "check_distance", null, [
      [ "Check_Distance", "classcheck__distance_1_1Check__Distance.html", "classcheck__distance_1_1Check__Distance" ]
    ] ],
    [ "closedloopcontroller", null, [
      [ "ClosedLoopController", "classclosedloopcontroller_1_1ClosedLoopController.html", "classclosedloopcontroller_1_1ClosedLoopController" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "led", null, [
      [ "LED", "classled_1_1LED.html", "classled_1_1LED" ]
    ] ],
    [ "motor", null, [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "ultrasonic", null, [
      [ "Ultrasonic", "classultrasonic_1_1Ultrasonic.html", "classultrasonic_1_1Ultrasonic" ]
    ] ],
    [ "update_debouncing", null, [
      [ "Update_Debouncing", "classupdate__debouncing_1_1Update__Debouncing.html", "classupdate__debouncing_1_1Update__Debouncing" ]
    ] ],
    [ "update_motor", null, [
      [ "Update_Motor", "classupdate__motor_1_1Update__Motor.html", "classupdate__motor_1_1Update__Motor" ]
    ] ],
    [ "update_spintray", null, [
      [ "Update_Spintray", "classupdate__spintray_1_1Update__Spintray.html", "classupdate__spintray_1_1Update__Spintray" ]
    ] ]
];